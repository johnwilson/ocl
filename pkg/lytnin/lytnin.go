package lytnin

import (
	"fmt"
	"io/ioutil"
	"log"
	"path"
	"sync"

	"github.com/BurntSushi/toml"
	"github.com/bamzi/jobrunner"
	"github.com/kelseyhightower/envconfig"
	"github.com/labstack/echo"
	mw "github.com/labstack/echo/middleware"
	"github.com/urfave/cli"
)

// Addon implements new application functionality
type Addon interface {
	Root() string
	Setup(l *Lytnin)
}

// AppConfig is the default application configuration
type AppConfig struct {
	Host                string `envconfig:"HOST"`
	Port                int    `envconfig:"PORT" default:"9000"`
	JobPoolSize         int    `envconfig:"JOB_POOL_SIZE" default:"1"`
	Debug               bool   `envconfig:"APPLICATION_DEBUG" default:"true"`
	HttpStatic          string `envconfig:"HTTP_STATIC" default:"web/static"`
	TemplatesDir        string `envconfig:"HTTP_TEMPLATES_DIR" default:"web/templates"`
	CookieAuthSecret    string `envconfig:"COOKIE_AUTH_SECRET"`
	CookieEncryptSecret string `envconfig:"COOKIE_ENCRYPT_SECRET"`
	JWTDuration         string `envconfig:"JWT_DURATION" default:"24h"`
	CookieDuration      string `envconfig:"COOKIE_DURATION" default:"30m"`
	JWTSecret           string `envconfig:"JWT_SECRET"`
	MailUsername        string `envconfig:"MAIL_USERNAME"`
	MailPassword        string `envconfig:"MAIL_PASSWORD"`
	DatabaseURL         string `envconfig:"DATABASE_URL"`
	HttpErrorFilesDir   string `envconfig:"HTTP_ERROR_FILES_DIR" default:"web/error"`
	HttpLoginURL        string `envconfig:"HTTP_LOGIN_URL" default:"/login"`
}

type Manifest struct {
	Template *struct {
		Prefix string
		Dir    string
	}
	Files []struct {
		Path string
		URL  string `toml:"url"`
	}
}

// Lytnin contains application values and methods
type Lytnin struct {
	addons         []Addon
	cmd            *cli.App
	services       map[string]interface{}
	Echo           *echo.Echo
	Config         AppConfig
	renderEngine   *DefaultEngine
	sideNavigation Menu
}

// RegisterAddon adds new functionality to the app by registering addon
func (l *Lytnin) RegisterAddon(a Addon) {
	// add to registry
	l.addons = append(l.addons, a)
}

func (l *Lytnin) initAddon(a Addon) {
	// parse addon directory
	manifest := path.Join(a.Root(), "manifest.toml")
	b, err := ioutil.ReadFile(manifest)
	if err != nil {
		log.Fatal(err)
	}

	m := Manifest{}
	if err := toml.Unmarshal(b, &m); err != nil {
		log.Fatal(err)
	}

	// parse templates if any
	if m.Template != nil {
		pth := path.Join(a.Root(), m.Template.Dir)
		l.addTemplates(m.Template.Prefix, pth)
	}

	// add static files if any
	for _, item := range m.Files {
		pth := path.Join(a.Root(), item.Path)
		_url := path.Join("/", a.Root(), item.URL)
		l.addFile(_url, pth)
	}
}

// LoadEnv enables addons to access environment variables
func (l *Lytnin) LoadEnv(prefix string, c interface{}) {
	// get general env
	err := envconfig.Process(prefix, c)
	if err != nil {
		log.Fatal(err.Error())
	}
}

var mutex sync.RWMutex

// AddService adds application dependencies/plugins
func (l *Lytnin) AddService(name string, s interface{}) {
	mutex.Lock()
	l.services[name] = s
	mutex.Unlock()
}

// GetService returns existing application dependencies/plugins
func (l *Lytnin) GetService(name string) interface{} {
	mutex.RLock()
	var s interface{}
	s, _ = l.services[name]
	mutex.RUnlock()
	return s
}

// AddCommand adds a new cli subcommand
func (l *Lytnin) AddCommand(cmd ...cli.Command) {
	for _, item := range cmd {
		l.cmd.Commands = append(l.cmd.Commands, item)
	}
}

// AddToSideNav add a menu item to the side navigation bar
func (l *Lytnin) AddToSideNav(i MenuItem) {
	l.sideNavigation.Add(i)
}

// addTemplates parses addon's templates and includes
// them with the default templates
func (l *Lytnin) addTemplates(prefix, dir string) {
	res := getTemplateFiles(dir, prefix)
	for _, item := range res {
		s := string(item.Content)
		tmpl := l.renderEngine.templates.New(item.Name)
		_, err := tmpl.Parse(s)
		if err != nil {
			panic(err)
		}
	}
}

// addFile registers a file to serve as a static asset
func (l *Lytnin) addFile(path, file string) {
	l.Echo.File(path, file)
}

// web is the default cli command
func (l *Lytnin) web(ctx *cli.Context) error {
	// start job runner
	jobrunner.Start(l.Config.JobPoolSize)

	// get address
	addr := fmt.Sprintf("%s:%d", l.Config.Host, l.Config.Port)

	// start server
	l.Echo.Logger.Fatal(l.Echo.Start(addr))

	return nil
}

// Start launches the application from the command line
func (l *Lytnin) Start(args []string) error {
	// get general env
	err := envconfig.Process("", &l.Config)
	if err != nil {
		log.Fatal(err.Error())
	}

	l.Echo.Debug = l.Config.Debug

	// middleware
	l.Echo.Use(mw.LoggerWithConfig(mw.LoggerConfig{
		Format: "method=${method}, uri=${uri}, status=${status}\n",
	}))

	// static assets
	l.Echo.Static(
		"/public",
		l.Config.HttpStatic,
	)

	// render templates
	re := NewDefaultEngine(l.Config.TemplatesDir)
	l.renderEngine = re
	l.Echo.Renderer = re

	// error handling
	h := ErrorHandler{
		FilesDir: l.Config.HttpErrorFilesDir,
		LoginURL: l.Config.HttpLoginURL,
	}
	l.Echo.HTTPErrorHandler = h.Error

	// initialize modules
	for _, a := range l.addons {
		l.initAddon(a)
		a.Setup(l)
	}

	return l.cmd.Run(args)
}

// LytninApp is the application instance
var LytninApp *Lytnin

func init() {
	LytninApp = &Lytnin{
		addons:         []Addon{},
		cmd:            cli.NewApp(),
		Echo:           echo.New(),
		sideNavigation: Menu{},
		services:       map[string]interface{}{},
	}

	// setup cli
	LytninApp.cmd.Name = "lytnin"
	LytninApp.cmd.Usage = "modular application construction kit"
	LytninApp.cmd.Action = LytninApp.web
	LytninApp.cmd.Commands = []cli.Command{}
}
