package lytnin

import (
	"html/template"
	"io"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/labstack/echo"
	"github.com/microcosm-cc/bluemonday"
	"gopkg.in/russross/blackfriday.v2"
)

type DefaultEngine struct {
	templates *template.Template
	rootDir   string
}

type TemplateData struct {
	Path    string
	Content []byte
	Name    string
}

func getTemplateFiles(dir, prefix string) []TemplateData {
	var res = []TemplateData{}

	err := filepath.Walk(dir, func(path string, f os.FileInfo, err error) error {
		if filepath.Ext(path) == ".html" {
			data, err := ioutil.ReadFile(path)
			if err != nil {
				return err
			}

			name := strings.TrimPrefix(path, dir)
			if len(prefix) > 0 {
				name = strings.TrimLeft(name, "/")
				name = prefix + "/" + name
			} else {
				name = strings.TrimLeft(name, "/")
			}

			td := TemplateData{
				Path:    path,
				Content: data,
				Name:    name,
			}

			res = append(res, td)
		}

		return nil
	})

	if err != nil {
		panic(err)
	}

	return res
}

func readTemplateFiles(dir string) *template.Template {
	res := getTemplateFiles(dir, "")
	var t *template.Template

	for _, item := range res {
		s := string(item.Content)
		var tmpl *template.Template
		if t == nil {
			t = template.New(item.Name)
		}
		if item.Name == t.Name() {
			tmpl = t
		} else {
			tmpl = t.New(item.Name)
		}
		_, err := tmpl.Parse(s)
		if err != nil {
			panic(err)
		}
	}
	return t
}

func NewDefaultEngine(dir string) *DefaultEngine {
	t := readTemplateFiles(dir)

	eng := &DefaultEngine{
		templates: t,
		rootDir:   dir,
	}

	// Add custom template functions
	t.Funcs(template.FuncMap{
		"markdown":  ToMarkdown,
		"torange":   ToRange,
		"linebreak": BreakLines,
	})

	return eng
}

// Render writes the template to the current request
func (e *DefaultEngine) Render(w io.Writer, name string, data interface{}, ctx echo.Context) error {
	if path.Ext(name) == "" {
		name += ".html"
	}
	return e.templates.ExecuteTemplate(w, name, data)
}

// ToMarkdown converts a string to markdown
// It accepts an interface{} as value in case
// a nil value is passed to the template
func ToMarkdown(val interface{}) template.HTML {
	s, ok := val.(string)
	if !ok {
		return template.HTML("")
	}

	unsafe := blackfriday.Run([]byte(s))
	html := bluemonday.UGCPolicy().SanitizeBytes(unsafe)
	return template.HTML(html)
}

func ToRange(val interface{}) []int64 {
	num, ok := val.(int64)
	if !ok {
		return []int64{}
	}

	l := []int64{}
	for i := int64(0); i < num; i++ {
		l = append(l, i)
	}

	return l
}

func BreakLines(val interface{}) template.HTML {
	s, ok := val.(string)
	if !ok {
		return template.HTML("")
	}
	s = strings.Replace(s, "\n", "<br>", -1)
	return template.HTML(s)
}
