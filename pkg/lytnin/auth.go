package lytnin

import (
	"crypto/rand"
	"fmt"
	"net/http"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/securecookie"
	"github.com/labstack/echo"
	mw "github.com/labstack/echo/middleware"
)

// PWGenAlpha creates a password of specified length using alpha characterset
func PWGenAlpha(l int) string {
	chars := "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	return generator(l, chars)
}

// PWGenAlphaNumeric creates a password of specified length using alpha-numeric characterset
func PWGenAlphaNumeric(l int) string {
	chars := "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	return generator(l, chars)
}

// PWGenQUERTY creates a password of specified length using 'querty' characterset
func PWGenQUERTY(l int) string {
	chars := "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-=_+{}[];':\"|<>,./?\\ "
	return generator(l, chars)
}

func generator(length int, charSet string) string {
	password := make([]byte, length)
	rand.Read(password)

	for k, v := range password {
		password[k] = charSet[v%byte(len(charSet))]
	}
	return string(password)
}

// AuthUser contains basic authenticated user information
type AuthUser struct {
	Username string `json:"username"`
	ID       int64  `json:"id"`
	Name     string `json:"name"`
	Admin    bool   `json:"admin"`
	Role     string `json:"role"`
}

// JWTCustomClaims contains jwt claim and user specific info to be encoded
type JWTCustomClaims struct {
	AuthUser
	jwt.StandardClaims
}

// Auth manages application authentication and authorization
type Auth struct {
	App *Lytnin
}

func (a *Auth) createSecureCookie() *securecookie.SecureCookie {
	s := securecookie.New(
		[]byte(a.App.Config.CookieAuthSecret),
		[]byte(a.App.Config.CookieEncryptSecret),
	)
	return s
}

// CreateAuthCookies creates 2 authentication cookies in the context
// cji: JWT Information cookie containing JWT header and payload
// cjs: JWT Signature cookie containing JWT signature and encrypted
func (a *Auth) CreateAuthCookies(usr AuthUser, ctx echo.Context) error {
	var cji = new(http.Cookie)
	var cjs = new(http.Cookie)

	jwtDuration, err := time.ParseDuration(a.App.Config.JWTDuration)
	if err != nil {
		return err
	}
	cookieDuration, err := time.ParseDuration(a.App.Config.CookieDuration)
	if err != nil {
		return err
	}

	sc := jwt.StandardClaims{
		ExpiresAt: time.Now().Add(jwtDuration).Unix(),
	}

	// create JWT
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &JWTCustomClaims{usr, sc})
	t, err := token.SignedString([]byte(a.App.Config.JWTSecret))
	if err != nil {
		return err
	}

	parts := strings.Split(t, ".")

	// create secure cookie
	s := a.createSecureCookie()

	// create cookies: signature
	cookieName := "jwt-sec2"
	var cookieVal string

	if cookieVal, err = s.Encode(cookieName, parts[2]); err != nil {
		return err
	}

	cjs.Name = cookieName
	cjs.Value = cookieVal
	cjs.HttpOnly = true

	// create cookies: header.payload
	cji.Name = "jwt-sec1"
	cji.Value = fmt.Sprintf("%s.%s", parts[0], parts[1])
	cji.Expires = time.Now().Add(cookieDuration)
	cji.Path = "/"
	cji.HttpOnly = false

	ctx.SetCookie(cjs)
	ctx.SetCookie(cji)

	return nil
}

// RemoveAuthCookies expires context auth cookies
// cji: JWT Information cookie containing JWT header and payload
// cjs: JWT Signature cookie containing JWT signature and encrypted
func (a *Auth) RemoveAuthCookies(ctx echo.Context) error {
	var cookie = new(http.Cookie)
	var cookie2 = new(http.Cookie)

	cookie.Name = "jwt-sec1"
	cookie.MaxAge = -1
	cookie2.Name = "jwt-sec2"
	cookie2.MaxAge = -1

	ctx.SetCookie(cookie)
	ctx.SetCookie(cookie2)

	return nil
}

// AssembleJWTMiddleware verifies if both cookies containing parts of the JWT token
// are present. If they are the JWT token is reassembled from the cookie values
func (a *Auth) AssembleJWTMiddleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			cookie1, err1 := c.Cookie("jwt-sec2")
			cookie2, err2 := c.Cookie("jwt-sec1")

			// decode signature cookie
			s := a.createSecureCookie()

			if err1 == nil && err2 == nil {
				var val string
				if err := s.Decode("jwt-sec2", cookie1.Value, &val); err == nil {
					jwt := fmt.Sprintf("Bearer %s.%s", cookie2.Value, val)
					c.Request().Header.Set("Authorization", jwt)
				}

			}

			return next(c)
		}
	}
}

// GetJWTUser gets current user in the context
func (a *Auth) GetJWTUser(ctx echo.Context) *AuthUser {
	// get jwt data set by JWT middleware in context
	jwt, ok := ctx.Get("current_user").(*jwt.Token)
	if !ok {
		return nil
	}

	claims, ok := jwt.Claims.(*JWTCustomClaims)
	if !ok {
		return nil
	}

	return &claims.AuthUser
}

// CreateAuthToken creates JWT
func (a *Auth) CreateAuthToken(usr AuthUser, ctx echo.Context) error {
	jwtDuration, err := time.ParseDuration(a.App.Config.JWTDuration)
	if err != nil {
		return err
	}

	sc := jwt.StandardClaims{
		ExpiresAt: time.Now().Add(jwtDuration).Unix(),
	}

	// create JWT
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &JWTCustomClaims{usr, sc})
	t, err := token.SignedString([]byte(a.App.Config.JWTSecret))
	if err != nil {
		return err
	}

	payload := map[string]interface{}{
		"token": t,
	}

	return ctx.JSON(http.StatusOK, payload)
}

// assembleJWT rebuilds the token from cookies and adds it to the request header
// before the JWT middleware evaluates its validity
func (a *Auth) assembleJWT(ctx echo.Context) {
	cookie1, err1 := ctx.Cookie("jwt-sec2")
	cookie2, err2 := ctx.Cookie("jwt-sec1")

	// decode signature cookie
	s := a.createSecureCookie()

	if err1 == nil && err2 == nil {
		var val string
		if err := s.Decode("jwt-sec2", cookie1.Value, &val); err == nil {
			jwt := fmt.Sprintf("Bearer %s.%s", cookie2.Value, val)
			ctx.Request().Header.Set("Authorization", jwt)
		}
	}
}

// authErrorHandler takes care of the JWT middleware authentication errors
func (a *Auth) authErrorHandler(err error) error {
	e, ok := err.(*echo.HTTPError)
	if ok {
		// change http code to unauthorized always
		e.Code = http.StatusUnauthorized
	}
	return e
}

// JWTMiddleware creates a custom JWT middleware to evaluate request tokens
func (a *Auth) JWTMiddleware() echo.MiddlewareFunc {
	jwtcfg := mw.JWTConfig{
		Claims:        &JWTCustomClaims{},
		SigningKey:    []byte(a.App.Config.JWTSecret),
		SigningMethod: "HS256",
		ContextKey:    "current_user",
		AuthScheme:    "Bearer",
		BeforeFunc:    a.assembleJWT,
		ErrorHandler:  a.authErrorHandler,
	}

	return mw.JWTWithConfig(jwtcfg)
}
