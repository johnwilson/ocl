package lytnin

// MenuItem represents a clickable link
type MenuItem struct {
	Link    string
	Title   string
	Tooltip string
	Submenu Menu
}

// Menu contains a list of menu items
type Menu struct {
	Items []MenuItem
}

// Add inserts a menu item into the menu
func (m Menu) Add(i MenuItem) {
	m.Items = append(m.Items, i)
}
