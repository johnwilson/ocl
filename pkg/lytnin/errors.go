package lytnin

import (
	"fmt"
	"net/http"
	"path"
	"strings"

	"github.com/labstack/echo"
)

type ErrorHandler struct {
	FilesDir string
	LoginURL string
}

func (h ErrorHandler) Error(err error, ctx echo.Context) {
	// default http error code
	code := http.StatusInternalServerError
	message := ""

	// check if error type is echo httperror
	if he, ok := err.(*echo.HTTPError); ok {
		code = he.Code
		message, ok = he.Message.(string)
	}

	if code == 401 || code == 400 {
		// check if jwt error
		if strings.Contains(message, "jwt") {
			if h.LoginURL == "" {

			}
			if err := ctx.Redirect(http.StatusFound, h.LoginURL); err != nil {
				ctx.Logger().Error(err)
			}
		}

	} else {
		errorPage := fmt.Sprintf("%d.html", code)
		pth := path.Join(h.FilesDir, errorPage)
		if err := ctx.File(pth); err != nil {
			ctx.Logger().Error(err)
		}
	}

	ctx.Logger().Error(err)
}
