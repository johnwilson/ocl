package ppme

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRolePermissions(t *testing.T) {
	assert := assert.New(t)

	errorMessage := func(perm Permission, expected interface{}, actual interface{}) string {
		return fmt.Sprintf(
			"Expected role.HasPermission: %d, to be %v but got %v instead",
			perm,
			expected,
			actual,
		)
	}

	r := Role{}
	cases := []struct {
		Perm     Permission
		Expected bool
	}{
		{CanAdminister, false},
		{CanLogin, false},
	}
	for _, tc := range cases {
		actual := r.HasPermission(tc.Perm)
		assert.Equal(
			tc.Expected,
			actual,
			errorMessage(tc.Perm, tc.Expected, actual),
		)
	}

	r.AddPermission(CanAdminister)
	cases = nil
	cases = []struct {
		Perm     Permission
		Expected bool
	}{
		{CanAdminister, true},
		{CanLogin, false},
	}
	for _, tc := range cases {
		actual := r.HasPermission(tc.Perm)
		assert.Equal(
			tc.Expected,
			actual,
			errorMessage(tc.Perm, tc.Expected, actual),
		)
	}

	r.AddPermission(CanLogin)
	cases = nil
	cases = []struct {
		Perm     Permission
		Expected bool
	}{
		{CanAdminister, true},
		{CanLogin, true},
	}
	for _, tc := range cases {
		actual := r.HasPermission(tc.Perm)
		assert.Equal(
			tc.Expected,
			actual,
			errorMessage(tc.Perm, tc.Expected, actual),
		)
	}

	r.RemovePermission(CanAdminister)
	cases = nil
	cases = []struct {
		Perm     Permission
		Expected bool
	}{
		{CanAdminister, false},
		{CanLogin, true},
	}
	for _, tc := range cases {
		actual := r.HasPermission(tc.Perm)
		assert.Equal(
			tc.Expected,
			actual,
			errorMessage(tc.Perm, tc.Expected, actual),
		)
	}

	r.ResetPermission()
	cases = nil
	cases = []struct {
		Perm     Permission
		Expected bool
	}{
		{CanAdminister, false},
		{CanLogin, false},
	}
	for _, tc := range cases {
		actual := r.HasPermission(tc.Perm)
		assert.Equal(
			tc.Expected,
			actual,
			errorMessage(tc.Perm, tc.Expected, actual),
		)
	}
}
