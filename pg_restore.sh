dump_file=
db="ppme"
user="ppme"

while [ "$1" != "" ]; do
    case $1 in
        -f | --file )       shift
                            dump_file=$1
                            ;;
        -d | --database )   shift
                            db=$1
                            ;;
        -u | --user )       shift
                            user=$1
                            ;;
    esac
    shift
done

echo
echo "starting restore"
echo
# restore database dump
pg_restore --clean --no-acl --no-owner -x -d $db $dump_file

# restore privileges
psql -d $db -c "GRANT ALL PRIVILEGES ON SCHEMA public TO $user;"
psql -d $db -c "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO $user;"
psql -d $db -c "GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO $user;"
psql -d $db -c "GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public TO $user"
psql -d $db -c "GRANT USAGE ON SCHEMA public TO $user"

echo
echo "...done"
echo