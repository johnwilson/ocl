#!make
include .env

cwd = $(shell pwd)
migration_dir = "$(cwd)/sql/migrations"
cli = "$(cwd)/cmd/oclnow/main.go"

all: test build

dev:
	gin -d cmd/oclnow -b oclnow --all -x web/static -x tmp -x vendor run cmd/oclnow/main.go

build:
	@echo "building..."
	go build -o oclnow $(cli)

setup:
	dep ensure

test:
	ENV=TESTINNG go test -failfast -v ./...

clean:
	rm ./oclnow

deploy-live:
	dokku --app ocl config:set --no-restart\
		MAIL_USERNAME="$(MAIL_USERNAME)" \
		MAIL_PASSWORD="$(MAIL_PASSWORD)" \
		COOKIE_AUTH_SECRET="$(COOKIE_AUTH_SECRET)" \
		COOKIE_ENCRYPT_SECRET="$(COOKIE_ENCRYPT_SECRET)" \
		JWT_SECRET="$(JWT_SECRET)" \
		CMS_ROOT_DIR="$(CMS_ROOT_DIR)" \
		JWT_DURATION="$(JWT_DURATION)" \
		COOKIE_DURATION="$(COOKIE_DURATION)" \
		APPLICATION_DEBUG=false
	git push --force live master

deploy:
	dokku --app ocl-staging config:set --no-restart\
		MAIL_USERNAME="$(MAIL_USERNAME)" \
		MAIL_PASSWORD="$(MAIL_PASSWORD)" \
		COOKIE_AUTH_SECRET="$(COOKIE_AUTH_SECRET)" \
		COOKIE_ENCRYPT_SECRET="$(COOKIE_ENCRYPT_SECRET)" \
		JWT_SECRET="$(JWT_SECRET)" \
		CMS_ROOT_DIR="$(CMS_ROOT_DIR)" \
		JWT_DURATION="$(JWT_DURATION)" \
		COOKIE_DURATION="$(COOKIE_DURATION)" \
		APPLICATION_DEBUG=false
	git push --force staging master

migrateup:
	dbmate -d "./sql/migrations" -s "./sql/schema/schema.sql" up

migratedown:
	dbmate -d "./sql/migrations" -s "./sql/schema/schema.sql" down

migrate:
	dbmate -d "./sql/migrations" -s "./sql/schema/schema.sql" migrate
