package site

import (
	"github.com/labstack/echo"
)

func (a *Site) homePage(ctx echo.Context) error {
	p, _ := a.cms.Page(
		"index.toml",
		"common/testimonials.toml",
	)
	data := map[string]interface{}{
		"WhiteNav": true,
		"CMS":      p,
	}
	return ctx.Render(200, "site/index", data)
}

func (a *Site) faqPage(ctx echo.Context) error {
	p, _ := a.cms.Page(
		"faq.toml",
	)
	data := map[string]interface{}{
		"WhiteNav": true,
		"CMS":      p,
	}
	return ctx.Render(200, "site/faq", data)
}

func (a *Site) pricingPage(ctx echo.Context) error {
	p, _ := a.cms.Page(
		"pricing.toml",
	)
	data := map[string]interface{}{
		"WhiteNav": true,
		"CMS":      p,
	}
	return ctx.Render(200, "site/pricing", data)
}

func (a *Site) supportPage(ctx echo.Context) error {
	p, _ := a.cms.Page(
		"support.toml",
	)
	data := map[string]interface{}{
		"WhiteNav": true,
		"CMS":      p,
	}
	return ctx.Render(200, "site/support", data)
}
