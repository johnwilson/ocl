package site

import (
	"oclnow/addons/cms"
	"oclnow/pkg/lytnin"
)

// Config is the addon's configuration
type Config struct {
}

// Site handles the functionality for the addon
type Site struct {
	config Config
	cms    *cms.CMS
	app    *lytnin.Lytnin
}

// Root returns the path of the addon relative to the main root directory
func (a *Site) Root() string {
	return "addons/site"
}

// Setup initializes the addon
func (a *Site) Setup(l *lytnin.Lytnin) {
	// load env vars
	l.LoadEnv("site", &a.config)

	// get services
	a.cms = l.GetService("cms").(*cms.CMS)
	a.app = l

	// create routes
	l.Echo.GET("/", a.homePage)
	l.Echo.GET("/faq", a.faqPage)
	l.Echo.GET("/prices", a.pricingPage)
	l.Echo.GET("/support", a.supportPage)
}

func init() {
	lytnin.LytninApp.RegisterAddon(&Site{})
}
