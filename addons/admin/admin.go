package admin

import (
	"ppme/pkg/lytnin"

	"github.com/labstack/echo"
)

// Admin handles the functionality for the addon
type Admin struct {
}

// Root returns the path of the addon relative to the main root directory
func (a *Admin) Root() string {
	return "addons/admin"
}

// Setup initializes the addon
func (a *Admin) Setup(l *lytnin.Lytnin) {
	// create routes
	l.Echo.GET("/admin", a.index)
}

func init() {
	lytnin.LytninApp.RegisterAddon(&Admin{})
}

func (a *Admin) index(ctx echo.Context) error {
	return ctx.Render(200, "admin/index", nil)
}
