const devMode = process.env.NODE_ENV !== "production"
const path = require("path")

module.exports = {
    watch: devMode,
    watchOptions: {
        ignored: ["node_modules"]
    },
    mode: devMode? "development" : "production",
    entry: {
        admin: "./src/admin/index.js"
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "[name].bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.tag$/,
                exclude: /node_modules/,
                use: [{
                    loader: "riot-tag-loader",
                    options: {
                        hot: false,
                        sourcemap: false
                    }
                }]
            }
        ]
    },
    plugins: []
}
