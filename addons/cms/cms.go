package cms

import (
	"oclnow/pkg/lytnin"
	"os"
	"path"

	"github.com/BurntSushi/toml"
	"github.com/imdario/mergo"
)

// PageInfo contains the TOML page definition
type PageInfo map[string]interface{}

// Config is the addon's configuration
type Config struct {
	RootDir string `envconfig:"ROOT_DIR" default:"web/cms"`
}

// CMS handles the functionality for the addon
type CMS struct {
	cache    map[string]PageInfo
	useCache bool
	config   Config
}

// Page gets a CMS page definition in TOML format
func (a *CMS) Page(names ...string) (PageInfo, error) {
	var rep PageInfo

	for _, name := range names {
		if a.useCache {
			tmp, ok := a.cache[name]
			if ok {
				if err := mergo.Merge(&rep, tmp); err != nil {
					return rep, err
				}
				continue
			}
		}

		// check if file exists
		f := path.Join(a.config.RootDir, name)
		_, err := os.Stat(f)
		if err != nil {
			return rep, err
		}

		// get file content
		var tmp PageInfo
		_, err = toml.DecodeFile(f, &tmp)
		if err != nil {
			return rep, err
		}

		if err = mergo.Merge(&rep, tmp); err != nil {
			return rep, err
		}
	}

	return rep, nil
}

// Root returns the path of the addon relative to the main root directory
func (a *CMS) Root() string {
	return "addons/cms"
}

// Setup initializes the addon
func (a *CMS) Setup(l *lytnin.Lytnin) {
	// load env vars
	l.LoadEnv("cms", &a.config)

	// initialize cache
	a.cache = map[string]PageInfo{}
	a.useCache = !l.Config.Debug

	// add page service
	l.AddService("cms", a)
}

func init() {
	lytnin.LytninApp.RegisterAddon(&CMS{})
}
