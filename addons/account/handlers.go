package account

import (
	"fmt"
	"net/http"
	"oclnow/pkg/lytnin"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"

	"github.com/asaskevich/govalidator"
	"github.com/bamzi/jobrunner"
	"github.com/dchest/passwordreset"
	"github.com/labstack/echo"
)

type (
	// RegistrationData contains user registration information
	// This constitutes the validated data from the form
	RegistrationData struct {
		Title     string
		Firstname string
		Lastname  string
		Email     string
	}
)

func (a *Account) registrationPage(ctx echo.Context) error {
	// data := map[string]interface{}{
	// 	"WhiteNav":  true,
	// 	"TitleList": TitleList,
	// }
	// if err := ctx.Get("errors"); err != nil {
	// 	data["Errors"] = err
	// }
	// return ctx.Render(200, "account/register", data)
	return ctx.Redirect(http.StatusMovedPermanently, "http://freshlux.net/tracking/signup.php")
}

func (a *Account) registrationAction(ctx echo.Context) error {
	form := struct {
		Title     string `json:"title" form:"title" valid:"required"`
		Firstname string `json:"firstname" form:"firstname" valid:"required,length(1|255),printableascii"`
		Lastname  string `json:"lastname" form:"lastname" valid:"required,length(1|255),printableascii"`
		Email     string `json:"email" form:"email" valid:"email,required"`
		Email2    string `json:"email2" form:"email2" valid:"email,required"`
	}{}

	if err := ctx.Bind(&form); err != nil {
		ctx.Logger().Error(err)
		ctx.Set("errors", "There was a problem with your account creation. Please contact support.")
		return a.registrationPage(ctx)
	}

	// validate
	if _, err := govalidator.ValidateStruct(form); err != nil {
		ctx.Logger().Error(err)
		ctx.Set("errors", "Please make sure all required fields have been completed with valid information.")
		return a.registrationPage(ctx)
	}
	if form.Email != form.Email2 {
		ctx.Set("errors", "Please make sure Email addresses match.")
		return a.registrationPage(ctx)
	}

	// serialize and encode form data into base64 string
	reg := RegistrationData{form.Title, form.Firstname, form.Lastname, form.Email}
	s, err := urlEncode(reg)
	if err != nil {
		ctx.Logger().Error(err)
		ctx.Set("errors", "There was a problem with your account creation. Please contact support.")
		return a.registrationPage(ctx)
	}

	u, err := emailActivationURL(s, ctx)
	if err != nil {
		ctx.Logger().Error(err)
		ctx.Set("errors", "There was a problem with your account creation. Please contact support.")
		return a.registrationPage(ctx)
	}

	// send activation email job
	job := AccountActivationEmailJob{
		From:            "info@oclnow.com",
		To:              reg.Email,
		Firstname:       reg.Firstname,
		Lastname:        reg.Lastname,
		Title:           reg.Title,
		ActivationToken: u,
		MailUsername:    a.app.Config.MailUsername,
		MailPassword:    a.app.Config.MailPassword,
	}

	jobrunner.Now(job)

	return ctx.Redirect(http.StatusFound, "/register/completed")
}

func (a *Account) registrationCompletePage(ctx echo.Context) error {
	p, _ := a.cms.Page(
		"registration_complete.toml",
	)
	data := map[string]interface{}{
		"WhiteNav": true,
		"CMS":      p,
	}
	return ctx.Render(200, "account/registration_complete", data)
}

func (a *Account) registrationInvalidPage(ctx echo.Context) error {
	p, _ := a.cms.Page(
		"registration_complete.toml",
	)
	data := map[string]interface{}{
		"WhiteNav": true,
		"CMS":      p,
	}
	return ctx.Render(200, "account/registration_invalid", data)
}

func (a *Account) activateAccountPage(ctx echo.Context) error {
	param := ctx.QueryParam("q")
	if len(param) == 0 {
		ctx.Logger().Info("Invalid activation code")
		return ctx.Redirect(http.StatusFound, "/register/invalid")
	}

	reg, err := urlDecode(param)
	if err != nil {
		ctx.Logger().Error(err)
		return ctx.Redirect(http.StatusFound, "/register/invalid")
	}

	data := map[string]interface{}{
		"WhiteNav":         true,
		"TitleList":        TitleList,
		"RegistrationInfo": reg,
		"Countries":        CountryList,
		"Cities":           CityList,
	}
	if err := ctx.Get("errors"); err != nil {
		data["Errors"] = err
	}
	return ctx.Render(200, "account/activate_account", data)
}

func (a *Account) activateAccountAction(ctx echo.Context) error {
	param := ctx.QueryParam("q")
	if len(param) == 0 {
		ctx.Logger().Info("Invalid activation code")
		return ctx.Redirect(http.StatusFound, "/register/invalid")
	}

	reg, err := urlDecode(param)
	if err != nil {
		ctx.Logger().Error(err)
		return ctx.Redirect(http.StatusFound, "/register/invalid")
	}

	form := struct {
		Country  string `json:"country" form:"country" valid:"required"`
		City     string `json:"city" form:"city" valid:"required"`
		Address  string `json:"address" form:"address" valid:"required"`
		Address2 string `json:"address2" form:"address2"`
		Phone    string `json:"phone" form:"phone" valid:"required"`
		Phone2   string `json:"phone2" form:"phone2"`
	}{}

	if err := ctx.Bind(&form); err != nil {
		ctx.Logger().Error(err)
		ctx.Set("errors", "There was a problem with your account creation. Please contact support.")
		return a.activateAccountPage(ctx)
	}

	// validate
	if _, err := govalidator.ValidateStruct(form); err != nil {
		ctx.Logger().Error(err)
		ctx.Set("errors", "Please make sure all required fields have been completed with valid information.")
		return a.activateAccountPage(ctx)
	}

	// check country is valid
	foundCountry := false
	for _, item := range CountryList {
		if item.Value == form.Country {
			foundCountry = true
			break
		}
	}
	if !foundCountry {
		ctx.Logger().Error("Invalid Hub data")
		ctx.Set("errors", "Please make sure you have selected a valid country.")
		return a.activateAccountPage(ctx)
	}

	// create user pw
	pw := lytnin.PWGenAlphaNumeric(9)
	hash, err := bcrypt.GenerateFromPassword([]byte(pw), bcrypt.DefaultCost)
	if err != nil {
		ctx.Logger().Error(err)
		ctx.Set("errors", "Application Error. Please contact support")
		return a.activateAccountPage(ctx)
	}

	var hubs []Hub
	a.db.Find(&hubs)

	// create customer
	c := Customer{
		Firstname: reg.Firstname,
		Lastname:  reg.Lastname,
		Title:     reg.Title,
		Email:     reg.Email,
		Country:   form.Country,
		City:      form.City,
		Address:   form.Address,
		Address2:  form.Address2,
		Phone:     form.Phone,
		Phone2:    form.Phone2,
		Password:  string(hash),
		Hubs:      hubs,
	}

	accnum, err := createCustomerID(c)
	if err != nil {
		ctx.Logger().Error(err)
		ctx.Set("errors", "Application Error. Please contact support")
		return a.activateAccountPage(ctx)
	}
	c.AccountNumber = accnum

	if err := a.db.Create(&c).Error; err != nil {
		ctx.Logger().Error(err)
		ctx.Set("errors", "Application Error. Please contact support")
		return a.activateAccountPage(ctx)
	}

	// send activation email job
	link := fmt.Sprintf(
		"%s://%s/%s/",
		ctx.Scheme(),
		ctx.Request().Host,
		"app",
	)

	job := NewAccountEmailJob{
		From:         "info@oclnow.com",
		Customer:     c,
		Hubs:         hubs,
		PasswordRaw:  pw,
		AccountLogin: link,
		MailUsername: a.app.Config.MailUsername,
		MailPassword: a.app.Config.MailPassword,
	}

	jobrunner.Now(job)

	return ctx.Redirect(http.StatusFound, "/account/activate/completed")
}

func (a *Account) activationCompletePage(ctx echo.Context) error {
	p, _ := a.cms.Page(
		"activation_complete.toml",
	)
	data := map[string]interface{}{
		"WhiteNav": true,
		"CMS":      p,
	}
	return ctx.Render(200, "account/activation_complete", data)
}

func (a *Account) myProfilePage(ctx echo.Context) error {
	// var c Customer
	// var hubs []Hub

	// usr := a.auth.GetJWTUser(ctx)
	// if usr == nil {
	// 	ctx.Logger().Error("User is nil")
	// 	// show error page
	// }

	// if err := a.db.Where("id = ?", usr.ID).First(&c).Error; err != nil {
	// 	ctx.Logger().Error(err)
	// 	// show error page
	// }

	// if err := a.db.Find(&hubs).Error; err != nil {
	// 	ctx.Logger().Error(err)
	// 	// show error page
	// }

	// data := map[string]interface{}{
	// 	"WhiteNav": true,
	// 	"Customer": c,
	// 	"Hubs":     hubs,
	// }

	// // get current user from ctx
	// data["CurrentUser"] = usr

	// return ctx.Render(200, "account/acc_profile", data)
	return ctx.Redirect(http.StatusMovedPermanently, "http://freshlux.net/tracking/login.php")
}

func (a *Account) myPasswordUpdatePage(ctx echo.Context) error {
	var c Customer

	usr := a.auth.GetJWTUser(ctx)
	if usr == nil {
		ctx.Logger().Error("User is nil")
		// show error page
	}

	if err := a.db.Where("id = ?", usr.ID).First(&c).Error; err != nil {
		ctx.Logger().Error(err)
		// show error page
	}

	data := map[string]interface{}{
		"WhiteNav": true,
		"Customer": c,
	}

	// get current user from ctx
	data["CurrentUser"] = usr
	data["PWChangeSuccess"] = ctx.Get("pw_change_success")
	data["PWChangeError"] = ctx.Get("pw_change_error")

	return ctx.Render(200, "account/acc_password", data)
}

func (a *Account) myPasswordUpdateAction(ctx echo.Context) error {
	form := struct {
		OldPassword string `json:"oldpw" form:"oldpw"`
		Password    string `json:"newpw" form:"newpw"`
		Confirm     string `json:"newpw2" form:"newpw2"`
	}{}

	if err := ctx.Bind(&form); err != nil {
		ctx.Logger().Error(err)
		ctx.Set("pw_change_error", "Please fill in all the fields.")
		return a.myPasswordUpdatePage(ctx)
	}
	form.Password = strings.TrimSpace(form.Password)

	// validate
	if form.Password != form.Confirm {
		ctx.Set("pw_change_error", "Please make sure the new and confirm passwords are the same.")
		return a.myPasswordUpdatePage(ctx)
	}

	if len(form.Password) < 6 {
		ctx.Set("pw_change_error", "Please make sure the password has at least 6 characters.")
		return a.myPasswordUpdatePage(ctx)
	}

	usr := a.auth.GetJWTUser(ctx)
	if usr == nil {
		ctx.Logger().Error("User is nil")
		// show error page
	}

	// get customer from db
	var c Customer
	if err := a.db.Where("id = ?", usr.ID).First(&c).Error; err != nil {
		ctx.Logger().Error(err)
		ctx.Set("pw_change_error", "User not found. Please contact admin.")
		return a.myPasswordUpdatePage(ctx)
	}

	// check old password
	if err := bcrypt.CompareHashAndPassword([]byte(c.Password), []byte(form.OldPassword)); err != nil {
		ctx.Logger().Error(err)
		ctx.Set("pw_change_error", "Your old password is incorrect.")
		return a.myPasswordUpdatePage(ctx)
	}

	// change password
	hash, err := bcrypt.GenerateFromPassword([]byte(form.Password), bcrypt.DefaultCost)
	if err != nil {
		ctx.Logger().Error(err)
		ctx.Set("pw_change_error", "Application Error. Please contact support")
		return a.myPasswordUpdatePage(ctx)
	}
	c.Password = string(hash)
	if err := a.db.Save(c).Error; err != nil {
		ctx.Logger().Error(err)
		ctx.Set("pw_change_error", "Application Error. Please contact support")
		return a.myPasswordUpdatePage(ctx)
	}

	ctx.Set("pw_change_success", true)
	return a.myPasswordUpdatePage(ctx)
}

func (a *Account) loginPage(ctx echo.Context) error {
	// usr := a.auth.GetJWTUser(ctx)
	// if usr != nil {
	// 	return ctx.Redirect(http.StatusFound, "/app/")
	// }

	// data := map[string]interface{}{
	// 	"WhiteNav": true,
	// }
	// if err := ctx.Get("errors"); err != nil {
	// 	data["Errors"] = err
	// }
	// return ctx.Render(200, "account/acc_login", data)
	return ctx.Redirect(http.StatusMovedPermanently, "http://freshlux.net/tracking/login.php")
}

func (a *Account) loginAction(ctx echo.Context) error {
	form := struct {
		Email    string `json:"email" form:"email" valid:"email,required"`
		Password string `json:"password" form:"password" valid:"required"`
	}{}

	if err := ctx.Bind(&form); err != nil {
		ctx.Logger().Error(err)
		ctx.Set("errors", "Please check your email and password.")
		return a.loginPage(ctx)
	}

	// validate
	if _, err := govalidator.ValidateStruct(form); err != nil {
		ctx.Logger().Error(err)
		ctx.Set("errors", "Please check your email and password.")
		return a.loginPage(ctx)
	}

	// get customer from db
	var c Customer
	if err := a.db.Where("email = ?", form.Email).First(&c).Error; err != nil {
		ctx.Logger().Error(err)
		ctx.Set("errors", "Please check your email and password.")
		return a.loginPage(ctx)
	}

	// check password
	if err := bcrypt.CompareHashAndPassword([]byte(c.Password), []byte(form.Password)); err != nil {
		ctx.Logger().Error(err)
		ctx.Set("errors", "Please check your email and password.")
		return a.loginPage(ctx)
	}

	// create auth user
	usr := lytnin.AuthUser{
		Username: c.Email,
		ID:       int64(c.ID),
		Admin:    false,
		Name:     c.Firstname + " " + c.Lastname,
	}
	if err := a.auth.CreateAuthCookies(usr, ctx); err != nil {
		ctx.Logger().Error(err)
		ctx.Set("errors", "Application error. Please contact support.")
		return a.loginPage(ctx)
	}

	return ctx.Redirect(http.StatusFound, "/app/")
}

func (a *Account) passwordRecoveryPage(ctx echo.Context) error {
	usr := a.auth.GetJWTUser(ctx)
	if usr != nil {
		return ctx.Redirect(http.StatusFound, "/app/")
	}

	data := map[string]interface{}{
		"WhiteNav": true,
	}
	if err := ctx.Get("errors"); err != nil {
		data["Errors"] = err
	}
	return ctx.Render(200, "account/acc_password_recovery", data)
}

func (a *Account) passwordRecoveryAction(ctx echo.Context) error {
	form := struct {
		Email string `json:"email" form:"email" valid:"email,required"`
	}{}

	if err := ctx.Bind(&form); err != nil {
		ctx.Logger().Error(err)
		ctx.Set("errors", "Please check your email.")
		return a.passwordRecoveryPage(ctx)
	}

	// validate
	if _, err := govalidator.ValidateStruct(form); err != nil {
		ctx.Logger().Error(err)
		ctx.Set("errors", "Please check your email.")
		return a.passwordRecoveryPage(ctx)
	}

	// get customer from db
	var c Customer
	if err := a.db.Where("email = ?", form.Email).First(&c).Error; err != nil {
		// If email doesn't exist just log error
		ctx.Logger().Error(err)
		return ctx.Redirect(http.StatusFound, "/login")
	}

	// create a new password reset token
	tk := passwordreset.NewToken(
		c.Email,
		30*time.Minute,
		[]byte(c.Password),
		[]byte(a.app.Config.CookieEncryptSecret),
	)

	tkURL, err := passwordResetURL(tk, ctx)
	if err != nil {
		ctx.Logger().Error(err)
		return ctx.Redirect(http.StatusFound, "/login")
	}

	// send password reset request email job
	job := PasswordResetTokenEmailJob{
		From:          "info@oclnow.com",
		Customer:      c,
		ResetTokenURL: tkURL,
		MailUsername:  a.app.Config.MailUsername,
		MailPassword:  a.app.Config.MailPassword,
	}

	jobrunner.Now(job)

	return ctx.Redirect(http.StatusFound, "/login")
}

func (a *Account) passwordRecoveryTokenPage(ctx echo.Context) error {
	data := map[string]interface{}{
		"WhiteNav": true,
	}

	token := ctx.QueryParam("q")
	if len(token) == 0 {
		ctx.Logger().Error("Invalid password recovery token")
		data["Errors"] = "Invalid password recovery token"
		return ctx.Render(200, "account/password_token_recovery", data)
	}

	// function to retrieve customer from db based on email in token
	getPasswordHash := func(login string) ([]byte, error) {
		// get customer from db
		var c Customer
		if err := a.db.Where("email = ?", login).First(&c).Error; err != nil {
			// If email doesn't exist just log error
			return []byte(""), err
		}

		return []byte(c.Password), nil
	}

	login, err := passwordreset.VerifyToken(
		token,
		getPasswordHash,
		[]byte(a.app.Config.CookieEncryptSecret),
	)
	if err != nil {
		ctx.Logger().Error(err)
		data["Errors"] = "Invalid password recovery token"
		return ctx.Render(200, "account/password_token_recovery", data)
	}

	// create user pw
	pw := lytnin.PWGenAlphaNumeric(9)
	hash, err := bcrypt.GenerateFromPassword([]byte(pw), bcrypt.DefaultCost)
	if err != nil {
		ctx.Logger().Error(err)
		data["Errors"] = "Application Error. Please contact support"
		return ctx.Render(200, "account/password_token_recovery", data)
	}

	// reset user password
	var c Customer
	if err := a.db.Where("email = ?", login).First(&c).Error; err != nil {
		ctx.Logger().Error(err)
		data["Errors"] = "Invalid password recovery token"
		return ctx.Render(200, "account/password_token_recovery", data)
	}

	// change password
	c.Password = string(hash)
	if err := a.db.Save(c).Error; err != nil {
		ctx.Logger().Error(err)
		data["Errors"] = "Application Error. Please contact support"
		return ctx.Render(200, "account/password_token_recovery", data)
	}

	// send password change email job
	job := PasswordChangeEmailJob{
		From:         "info@oclnow.com",
		Customer:     c,
		Password:     pw,
		MailUsername: a.app.Config.MailUsername,
		MailPassword: a.app.Config.MailPassword,
	}

	jobrunner.Now(job)

	// return success message
	data["Message"] = "Your password has been updated. Please check your email for your new credentials"
	data["Error"] = ""
	return ctx.Render(200, "account/password_token_recovery", data)
}

func (a *Account) logoutAction(ctx echo.Context) error {
	// remove auth cookies
	err := a.auth.RemoveAuthCookies(ctx)
	if err != nil {
		ctx.Logger().Error(err)
	}

	return ctx.Redirect(http.StatusFound, "/") // 302
}
