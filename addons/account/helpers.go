package account

import (
	"bytes"
	"encoding/base64"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"hash/adler32"
	"log"
	"net/url"
	"text/template"
	"time"

	"github.com/go-mail/mail"
	"github.com/labstack/echo"
)

func urlEncode(v RegistrationData) (string, error) {
	// serialize and encode form data into base64 string
	b := bytes.Buffer{}
	en := gob.NewEncoder(&b)
	if err := en.Encode(v); err != nil {
		return "", err
	}
	s := base64.StdEncoding.EncodeToString(b.Bytes())

	// url encode string
	s = url.QueryEscape(s)
	return s, nil
}

func urlDecode(s string) (RegistrationData, error) {
	v := RegistrationData{}

	// unescape string
	s, err := url.QueryUnescape(s)
	if err != nil {
		return v, err
	}

	// base64 decode
	tmp, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		return v, err
	}

	// deserialize
	b := bytes.Buffer{}
	b.Write(tmp)
	dec := gob.NewDecoder(&b)
	if err = dec.Decode(&v); err != nil {
		return v, err
	}

	return v, nil
}

func emailActivationURL(data string, ctx echo.Context) (string, error) {
	link := fmt.Sprintf(
		"%s://%s/%s",
		ctx.Scheme(),
		ctx.Request().Host,
		"account/activate",
	)

	u, err := url.Parse(link)
	if err != nil {
		return "", err
	}

	q := u.Query()
	q.Set("q", data)
	u.RawQuery = q.Encode()

	return u.String(), nil
}

var activateAccountEmail = `
Dear {{.Title}} {{.Firstname}} {{.Lastname}},

We're really excited for you to join us!
You're just one click away from activating your account.

Complete the signup process by clicking the following link:
	
	{{.ActivationToken}}

Thank you for being a customer and welcome to the OCL family!

Best regards

The OCL team.
`

type AccountActivationEmailJob struct {
	From            string
	To              string
	Firstname       string
	Lastname        string
	Title           string
	ActivationToken string
	MailUsername    string
	MailPassword    string
}

func (j AccountActivationEmailJob) Run() {
	t := template.New("email")
	t, err := t.Parse(activateAccountEmail)
	if err != nil {
		log.Println(err)
		return
	}

	var b bytes.Buffer
	if err := t.Execute(&b, j); err != nil {
		log.Println(err)
		return
	}

	// send email
	m := mail.NewMessage()
	m.SetHeader("From", j.From)
	m.SetHeader("To", j.To)
	m.SetHeader("Subject", "Account Activation")
	m.SetBody("text/plain", b.String())

	d := mail.NewDialer("mail.gandi.net", 587, j.MailUsername, j.MailPassword)
	if err := d.DialAndSend(m); err != nil {
		log.Println(err)
	}

	log.Println("Activation email sent to:", j.To)
}

var newAccountEmail = `
Dear {{.Customer.Title}} {{.Customer.Firstname}} {{.Customer.Lastname}},


Thank you for completing your account activation with OCL.

Your account login details are as follows:


Email:
{{.Customer.Email}}

Password:
{{.PasswordRaw}}


You can now log into your account at the following link: {{.AccountLogin}}

Your primary hub information is as follow:


Name:
{{.Customer.Firstname}} {{.Customer.Lastname}}

Account Number:
{{.Customer.AccountNumber}}

Address:

{{range .Hubs}}
{{$.Customer.Firstname}} {{$.Customer.Lastname}}
{{$.Customer.AccountNumber}}
{{.DeliveryInfo}}

{{end}}


If you have any questions, please do not hesitate to contact us.

Best regards,

The OCL Team
`

type NewAccountEmailJob struct {
	From         string
	Customer     Customer
	Hubs         []Hub
	PasswordRaw  string
	AccountLogin string
	MailUsername string
	MailPassword string
}

func (j NewAccountEmailJob) Run() {
	t := template.New("email")
	t, err := t.Parse(newAccountEmail)
	if err != nil {
		log.Println(err)
		return
	}

	var b bytes.Buffer
	if err := t.Execute(&b, j); err != nil {
		log.Println(err)
		return
	}

	// send email
	m := mail.NewMessage()
	m.SetHeader("From", j.From)
	m.SetHeader("To", j.Customer.Email)
	m.SetHeader("Subject", "Welcome to OCL")
	m.SetBody("text/plain", b.String())

	d := mail.NewDialer("mail.gandi.net", 587, j.MailUsername, j.MailPassword)
	if err := d.DialAndSend(m); err != nil {
		log.Println(err)
	}

	log.Println("New Account email sent to:", j.Customer.Email)
}

func createCustomerID(c Customer) (uint32, error) {
	var id uint32

	v := struct {
		Email string    `json:"email"`
		Time  time.Time `json:"dt"`
		Name  string    `json:"name"`
	}{
		c.Email,
		time.Now(),
		c.Firstname + "." + c.Lastname,
	}

	b, err := json.Marshal(&v)
	if err != nil {
		return id, err
	}

	id = adler32.Checksum(b)
	return id, nil
}

var passwordResetTokenEmail = `
Dear {{.Customer.Title}} {{.Customer.Firstname}} {{.Customer.Lastname}},


You have received this email because someone requested a password reset for your account.
If you didn't make this request, please ignore this email.

Your password reset link is as follows:

{{.ResetTokenURL}}

Please copy and paste the above link in your browser and follow the instructions.

This password request is time limited and will expire after 30 minutes.

If you have any questions, please do not hesitate to contact us.

Best regards,

The OCL Team
`

type PasswordResetTokenEmailJob struct {
	From          string
	Customer      Customer
	ResetTokenURL string
	MailUsername  string
	MailPassword  string
}

func passwordResetURL(data string, ctx echo.Context) (string, error) {
	link := fmt.Sprintf(
		"%s://%s/%s",
		ctx.Scheme(),
		ctx.Request().Host,
		"recovery-token-verification",
	)

	u, err := url.Parse(link)
	if err != nil {
		return "", err
	}

	q := u.Query()
	q.Set("q", data)
	u.RawQuery = q.Encode()

	return u.String(), nil
}

func (j PasswordResetTokenEmailJob) Run() {
	t := template.New("email")
	t, err := t.Parse(passwordResetTokenEmail)
	if err != nil {
		log.Println(err)
		return
	}

	var b bytes.Buffer
	if err := t.Execute(&b, j); err != nil {
		log.Println(err)
		return
	}

	// send email
	m := mail.NewMessage()
	m.SetHeader("From", j.From)
	m.SetHeader("To", j.Customer.Email)
	m.SetHeader("Subject", "Password Reset Request")
	m.SetBody("text/plain", b.String())

	d := mail.NewDialer("mail.gandi.net", 587, j.MailUsername, j.MailPassword)
	if err := d.DialAndSend(m); err != nil {
		log.Println(err)
	}

	log.Println("Password Reset Request email sent to:", j.Customer.Email)
}

var passwordChangeEmail = `
Dear {{.Customer.Title}} {{.Customer.Firstname}} {{.Customer.Lastname}},


Your password reset request has been completed.

Your new password is as follows:

{{.Password}}

You can now proceed to access your account.

If you have any questions, please do not hesitate to contact us.

Best regards,

The OCL Team
`

type PasswordChangeEmailJob struct {
	From         string
	Customer     Customer
	Password     string
	MailUsername string
	MailPassword string
}

func (j PasswordChangeEmailJob) Run() {
	t := template.New("email")
	t, err := t.Parse(passwordChangeEmail)
	if err != nil {
		log.Println(err)
		return
	}

	var b bytes.Buffer
	if err := t.Execute(&b, j); err != nil {
		log.Println(err)
		return
	}

	// send email
	m := mail.NewMessage()
	m.SetHeader("From", j.From)
	m.SetHeader("To", j.Customer.Email)
	m.SetHeader("Subject", "Password Updated")
	m.SetBody("text/plain", b.String())

	d := mail.NewDialer("mail.gandi.net", 587, j.MailUsername, j.MailPassword)
	if err := d.DialAndSend(m); err != nil {
		log.Println(err)
	}

	log.Println("Password Updated email sent to:", j.Customer.Email)
}
