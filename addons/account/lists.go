package account

// TitleList contains user titles
var TitleList = []string{"Mr", "Mrs", "Ms", "Miss"}

// CountryList contains list of countries
var CountryList = []struct {
	Name  string
	Value string
}{
	{"Ghana", "GH"},
}

// HubList contains list of hubs/countries currently available
var HubList = []struct {
	Name  string
	Value string
}{
	{"United States of America", "USA"},
	{"United Kingdom", "UK"},
	{"United Arab Emirates (UAE)", "UAE"},
	{"India", "IN"},
}

// CityList contains list of cities
var CityList = []struct {
	Country string
	Name    string
	Value   string
}{
	{"GH", "Accra", "Accra"},
	{"GH", "Kumasi", "Kumasi"},
	{"GH", "Tamale", "Tamale"},
	{"GH", "Takoradi", "Takoradi"},
	{"GH", "Ashaiman", "Ashaiman"},
	{"GH", "Tema", "Tema"},
	{"GH", "Teshie Old Town", "Teshie Old Town"},
	{"GH", "Cape Coast", "Cape Coast"},
	{"GH", "Sekondi-Takoradi", "Sekondi-Takoradi"},
	{"GH", "Obuase", "Obuase"},
	{"GH", "Madina Estates", "Madina Estates"},
	{"GH", "Koforidua", "Koforidua"},
	{"GH", "Wa", "Wa"},
	{"GH", "Ejura", "Ejura"},
	{"GH", "Nungua", "Nungua"},
	{"GH", "Sunyani", "Sunyani"},
	{"GH", "Ho", "Ho"},
	{"GH", "Techiman", "Techiman"},
	{"GH", "Aflao", "Aflao"},
	{"GH", "Berekum", "Berekum"},
	{"GH", "Nkawkaw", "Nkawkaw"},
	{"GH", "Akim Oda", "Akim Oda"},
	{"GH", "Bawku", "Bawku"},
	{"GH", "Hohoe", "Hohoe"},
	{"GH", "Bolgatanga", "Bolgatanga"},
	{"GH", "Tafo", "Tafo"},
	{"GH", "Swedru", "Swedru"},
	{"GH", "Suhum", "Suhum"},
	{"GH", "Dome", "Dome"},
	{"GH", "Kintampo", "Kintampo"},
	{"GH", "Gbawe", "Gbawe"},
	{"GH", "Nsawam", "Nsawam"},
	{"GH", "Winneba", "Winneba"},
	{"GH", "Kasoa", "Kasoa"},
	{"GH", "Yendi", "Yendi"},
	{"GH", "Mampong", "Mampong"},
	{"GH", "Konongo", "Konongo"},
	{"GH", "Asamankese", "Asamankese"},
	{"GH", "Prestea", "Prestea"},
	{"GH", "Tarkwa", "Tarkwa"},
	{"GH", "Dunkwa", "Dunkwa"},
	{"GH", "Agogo", "Agogo"},
	{"GH", "Wenchi", "Wenchi"},
	{"GH", "Anloga", "Anloga"},
	{"GH", "Begoro", "Begoro"},
	{"GH", "Savelugu", "Savelugu"},
	{"GH", "Kpando", "Kpando"},
	{"GH", "Elmina", "Elmina"},
	{"GH", "Salaga", "Salaga"},
	{"GH", "Navrongo", "Navrongo"},
	{"GH", "Saltpond", "Saltpond"},
	{"GH", "Axim", "Axim"},
	{"GH", "Akwatia", "Akwatia"},
	{"GH", "Shama Junction", "Shama Junction"},
	{"GH", "Apam", "Apam"},
	{"GH", "Foso", "Foso"},
	{"GH", "Bibiani", "Bibiani"},
	{"GH", "Aburi", "Aburi"},
	{"GH", "Keta", "Keta"},
	{"GH", "Bechem", "Bechem"},
	{"GH", "Duayaw Nkwanta", "Duayaw Nkwanta"},
	{"GH", "Mumford", "Mumford"},
	{"GH", "Akropong", "Akropong"},
	{"GH", "Kibi", "Kibi"},
	{"GH", "Kete Krachi", "Kete Krachi"},
	{"GH", "Mpraeso", "Mpraeso"},
	{"GH", "Aboso", "Aboso"},
	{"GH", "Kpandae", "Kpandae"},
	{"GH", "Akim Swedru", "Akim Swedru"},
}
