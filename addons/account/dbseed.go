package account

// InitHubs initialises database with default values
func (a *Account) InitHubs() {
	// hubs
	a.db.Create(&Hub{
		Name:         "United Kingdom (UK)",
		ShortCode:    "UK",
		DeliveryInfo: "RIF Worldwide Plc\nC/O OCL GHANA\nWorldwide House\nBrooklands Close\nSunbury-on-Thames\nMiddx\nTW16 7DX\nTel: +44 (0) 1932 753 799",
		Country:      "UK",
	})

	a.db.Create(&Hub{
		Name:         "United States of America (USA)",
		ShortCode:    "USA",
		DeliveryInfo: "CBX Global\nC/O OCL GHANA\n4248 Piedmont Parkway\nGreensboro\nNC 27410\nTel: +1 336 315 0443",
		Country:      "USA",
	})

	a.db.Create(&Hub{
		Name:         "India",
		ShortCode:    "IN",
		DeliveryInfo: "PXC Pacific Global Ltd\nC/O OCL GHANA\nA/3 Saubhagya Building\nJeevan Vikas Kendra Marg\nKoldongri, Andheri\nMumbai 400 069\nIndia\nTel: +912226838010 / 8020/",
		Country:      "India",
	})

	a.db.Create(&Hub{
		Name:         "United Arab Emirates (Dubai)",
		ShortCode:    "UAE",
		DeliveryInfo: "Orbit Shipping Services LLc\nC/O OCL GHANA\nG01 Al Asmawi Buidling\nDubai Investment Park\nPO #184820\nDubai. UAE\nTel: +97148877931",
		Country:      "UAE",
	})

	a.db.Create(&Hub{
		Name:         "China",
		ShortCode:    "CN",
		DeliveryInfo: "OCL Ghana c/o Wells shipping\nRoom 225, floor 2, building 4,\nXiaxue Village Science Park,\nXuegang South Road\nXuexiang Community\nBantian Street,\nShenzhen",
		Country:      "China",
	})
}
