package account

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" // postgres driver
	"github.com/urfave/cli"

	"oclnow/addons/cms"
	"oclnow/pkg/lytnin"
)

// Config is the addon's configuration
type Config struct {
}

// Account handles the functionality for the addon
type Account struct {
	config Config
	app    *lytnin.Lytnin
	db     *gorm.DB
	cms    *cms.CMS
	auth   lytnin.Auth
}

// Root returns the path of the addon relative to the main root directory
func (a *Account) Root() string {
	return "addons/account"
}

// Setup initializes the addon
func (a *Account) Setup(l *lytnin.Lytnin) {
	a.app = l
	a.auth = lytnin.Auth{App: l}

	// load env vars
	l.LoadEnv("account", &a.config)

	// setup database
	var db *gorm.DB
	var err error
	if db, err = gorm.Open("postgres", l.Config.DatabaseURL); err != nil {
		panic(err)
	}
	a.db = db

	// perform any outstanding migrations
	a.db.AutoMigrate(
		&Hub{},
		&Customer{},
	)

	// add command
	cmd := cli.Command{
		Name: "initdb",
		Action: func(ctx *cli.Context) error {
			a.InitHubs()
			return nil
		},
	}
	l.AddCommand(cmd)

	// get services
	a.cms = l.GetService("cms").(*cms.CMS)

	// create routes
	l.Echo.GET("/register", a.registrationPage)
	l.Echo.POST("/register", a.registrationAction)
	l.Echo.GET("/register/completed", a.registrationCompletePage)
	l.Echo.GET("/register/invalid", a.registrationInvalidPage)
	l.Echo.GET("/account/activate", a.activateAccountPage)
	l.Echo.POST("/account/activate", a.activateAccountAction)
	l.Echo.GET("/account/activate/completed", a.activationCompletePage)
	l.Echo.GET("/login", a.loginPage)
	l.Echo.POST("/login", a.loginAction)
	l.Echo.GET("/password-recovery", a.passwordRecoveryPage)
	l.Echo.POST("/password-recovery", a.passwordRecoveryAction)
	l.Echo.GET("/recovery-token-verification", a.passwordRecoveryTokenPage)
	l.Echo.GET("/logout", a.logoutAction)

	// account routes
	r := l.Echo.Group("/app")
	r.Use(
		a.auth.JWTMiddleware(),
	)

	r.GET("/", a.myProfilePage)
	r.GET("/password-update", a.myPasswordUpdatePage)
	r.POST("/password-update", a.myPasswordUpdateAction)
}

// Cleanup performs any cleanup activities the addon requires
func (a *Account) Cleanup(l *lytnin.Lytnin) {
	a.db.Close()
}

func init() {
	lytnin.LytninApp.RegisterAddon(&Account{})
}
