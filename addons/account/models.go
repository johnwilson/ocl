package account

import (
	"github.com/jinzhu/gorm"
)

type Hub struct {
	gorm.Model
	Name         string `gorm:"unique;not null"`
	ShortCode    string `gorm:"not null"`
	DeliveryInfo string `gorm:"not null"`
	Country      string `gorm:"not null"`
}

type Customer struct {
	gorm.Model
	AccountNumber uint32 `gorm:"unique_index;not null"`
	Firstname     string `gorm:"not null"`
	Lastname      string `gorm:"not null"`
	Title         string `gorm:"not null"`
	Email         string `gorm:"unique_index;not null"`
	Country       string `gorm:"not null"`
	City          string `gorm:"not null"`
	Address       string `gorm:"not null"`
	Address2      string
	Phone         string `gorm:"not null"`
	Phone2        string `gorm:"not null"`
	Password      string `gorm:"not null"`
	Hubs          []Hub  `gorm:"many2many:customer_hubs;"`
}
