package main

import (
	"log"
	"os"

	"github.com/joho/godotenv"

	_ "oclnow/addons/account"
	_ "oclnow/addons/site"
	"oclnow/pkg/lytnin"
)

func main() {
	// load env file if any
	godotenv.Load(".env")

	// start app
	l := lytnin.LytninApp
	if err := l.Start(os.Args); err != nil {
		log.Fatal(err)
	}
}
