package ppme

import (
	"time"

	"github.com/asaskevich/govalidator"
)

// MembershipService manages application users, roles, authentication and authorization
type MembershipService interface {
	// Roles
	CreateInitialRoles() error
	SaveRole(*Role) error
	SetDefaultRole(*Role) error
	GetDefaultRole() (*Role, error)
	GetRoleById(id int64) (*Role, error)
	GetAllRoles() ([]Role, error)
	GetAdminRole() (*Role, error)

	// Users
	RegisterUser(*UserRegistration) error
	GetUserByID(int64) (*UserSummary, error)
	GetUserByUsername(string) (*UserSummary, error)
	ValidateEmail(string) error
	CreateAdminUser(*UserRegistration) error
	GetAllUsers() ([]UserListItem, error)
	Authenticate(string, string) (*UserSummary, error)
	AddUserToRole(*UserSummary, *Role) error
	SaveUser(*UserUpdate) error
	GetEditableUser(int64) (*UserUpdate, error)
	UpdateUserStatus(status UserStatusAction, id int64, reason string) (*UserSummary, error)
	SetUserPassword(id int64, newpass string) error

	// Logs
	GetLogs() ([]MembershipLog, error)
}

// Permission handles user access
type Permission int64

const (
	// CanAdminister grants administration rights
	CanAdminister Permission = 1 << iota
	// CanLogin grants login rights
	CanLogin
)

// PermissionList contains the default application permissions
var PermissionList = map[string]Permission{
	"can administer": CanAdminister,
	"can login":      CanLogin,
}

type UserStatusAction string

const (
	UserSuspend  UserStatusAction = "suspended"
	UserLock                      = "locked"
	UserBan                       = "banned"
	UserActivate                  = "active"
)

type UserStatusActionListItem struct {
	Text          string
	Action        UserStatusAction
	AppliedStatus string
}

var UserStatusActionList = []UserStatusActionListItem{
	{
		"Suspend User",
		UserSuspend,
		"Suspended",
	},
	{
		"Lock User",
		UserLock,
		"Locked",
	},
	{
		"Ban User",
		UserBan,
		"Banned",
	},
	{
		"Activate User",
		UserActivate,
		"Active",
	},
}

type UserRegistration struct {
	Username  string `valid:"required,length(3|255),alphanum,lowercase"`
	Firstname string `valid:"required,length(1|255),utfletternum"`
	Lastname  string `valid:"required,length(1|255),utfletternum"`
	Email     string `valid:"email,required"`
	Password  string `valid:"required,printableascii,length(6|128)"`
}

type UserUpdate struct {
	ID        int64  `valid:"required" json:"id"`
	Firstname string `valid:"required,length(1|255),utfletternum" json:"first" db:"first"`
	Lastname  string `valid:"required,length(1|255),utfletternum" json:"last" db:"last"`
	Email     string `valid:"email,required" json:"email"`
	Role      int64  `valid:"required" json:"role"`
}

func (u *UserUpdate) Validate() error {
	_, err := govalidator.ValidateStruct(u)
	return err
}

type UserSummary struct {
	ID                   int64      `db:"id"`
	Username             string     `db:"username"`
	Email                string     `db:"email"`
	Status               string     `db:"status"`
	CanLogin             bool       `db:"can_login"`
	DisplayName          string     `db:"display_name"`
	UserKey              string     `db:"user_key"`
	EmailValidationToken string     `db:"email_validation_token"`
	Role                 string     `db:"role_name"`
	Permissions          Permission `db:"role_permissions"`
}

type UserListItem struct {
	ID          int64  `db:"id"`
	Username    string `db:"username"`
	Email       string `db:"email"`
	Status      string `db:"status"`
	CanLogin    bool   `db:"can_login"`
	DisplayName string `db:"display_name"`
	Role        string `db:"role_name"`
}

func (ur *UserRegistration) Validate() error {
	_, err := govalidator.ValidateStruct(ur)
	return err
}

func ValidatePassword(pw string) error {
	item := struct {
		Password string `valid:"required,printableascii,length(6|128)"`
	}{pw}
	_, err := govalidator.ValidateStruct(item)
	return err
}

// Role manages application user roles
type Role struct {
	ID          int64      `db:"id"`
	Name        string     `db:"name"`
	Permissions Permission `db:"permissions"`
}

// AddPermission adds Permission p to role
func (r *Role) AddPermission(p Permission) {
	if !r.HasPermission(p) {
		r.Permissions += p
	}
}

// RemovePermission removes Permission p from role
func (r *Role) RemovePermission(p Permission) {
	if r.HasPermission(p) {
		r.Permissions -= p
	}
}

// ResetPermission removes all permissions from role
func (r *Role) ResetPermission() {
	r.Permissions = 0
}

// HasPermission checks if a role's permissions includes permission p
func (r *Role) HasPermission(p Permission) bool {
	return (r.Permissions & p) == p
}

type MembershipLog struct {
	ID        int64     `db:"id"`
	Subject   string    `db:"subject"`
	Username  string    `db:"user"`
	Entry     string    `db:"entry"`
	Timestamp time.Time `db:"created_at"`
}
